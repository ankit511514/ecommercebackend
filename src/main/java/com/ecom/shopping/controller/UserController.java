package com.ecom.shopping.controller;

import com.ecom.shopping.modal.User;
import com.ecom.shopping.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@CrossOrigin(origins = "http://localhost:4200",allowedHeaders = "*")
@RequestMapping(value = "/")
public class UserController {

    @Autowired
    UserService userService;
    @PostMapping(path = "/adduser",produces = "application/json",consumes = "application/json")
    public User addUser(@RequestBody User user)
    {
        return userService.addUser(user);
    }
    @GetMapping(path ="/checkUser",produces = "application/json")
    public String checkUser() {return "\"valid\"";}
    @GetMapping("/callUser")
    public User callUsers(Principal principal)
    {
        return userService.callUser(principal);
    }

    @PostMapping("/editUser")
    public User editUsers(@RequestBody User user)
    {
        return userService.changeUserDetails(user);
    }

}
