package com.ecom.shopping.controller;

import com.ecom.shopping.modal.Products;
import com.ecom.shopping.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:4200",allowedHeaders = "*")
@RestController
@RequestMapping(value = "/callproduct",method = {RequestMethod.DELETE,RequestMethod.GET,RequestMethod.POST})
public class ProductsController {
    @Autowired
    ProductsService productsService;
    @GetMapping("/show")
    public List<Products> show()
    {
        return productsService.show();
    }
    @GetMapping("/showbyid/{id}")
    public Products showbyid(@PathVariable("id")int id)
    {
        return productsService.showbyid(id);
    }

    @PostMapping("/put")
    public Products put(@RequestBody Products products)
    {
        return productsService.put(products);
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id")int id)
    {
        return productsService.delete(id);
    }
    @GetMapping("/deleteall")
    public String deleteall(){
        return productsService.deleteall();
    }
    @GetMapping("/showbycategory/{category}")
    public List<Products> showbyid(@PathVariable("category")String category)
    {
        return productsService.showbycategory(category);
    }
    @GetMapping("/products-cat/{category}/{min}/{max}")
    public List<Products> getProductsByPrice(@PathVariable("category") String category,@PathVariable("min") double min,@PathVariable("max") double max)
    {
        return productsService.getProductByCategoryAndPrice(category, min, max);
    }
    @GetMapping("/all-products/{min}/{max}")
    public List<Products> getProductsByPrice(@PathVariable("min") double min,@PathVariable("max") double max)
    {
        return productsService.getProductByPrice(min, max);
    }
    @GetMapping("/products")
    public List<Products> getAllProducts() { return productsService.getProductList(); }
    @PostMapping("/editProduct")
    public Products editUsers(@RequestBody Products products)
    {
        return productsService.changeProductDescriptions(products);
    }

    @GetMapping("/search/{searchedItem}")
    public Set<Products> searchItem(@PathVariable("searchedItem") String searchedItem) {
        Set<Products> prod = productsService.getSearchedData(searchedItem);
        for (int i = 0; i < prod.size(); i++) {
            System.out.println(prod);
        }
        return prod;
    }
}
