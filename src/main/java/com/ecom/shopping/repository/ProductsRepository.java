package com.ecom.shopping.repository;

import com.ecom.shopping.modal.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductsRepository extends JpaRepository<Products,Integer> {

    List<Products> findByCategory(String category);
    List<Products> findByPriceBetween(double min, double max);
    List<Products> findByCategoryAndPriceBetween(String category, double min, double max);
    Products findById(int productId);
}
