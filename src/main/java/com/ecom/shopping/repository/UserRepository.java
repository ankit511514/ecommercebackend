package com.ecom.shopping.repository;

import com.ecom.shopping.modal.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    User findByEmail(String email);
    User findByUserid(int id);

    User getByEmail(String name);
}
