package com.ecom.shopping.service;

import com.ecom.shopping.modal.User;
import com.ecom.shopping.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    public User addUser(User user)
    {
        return userRepository.save(user);
    }

    public Integer getUserIds(Principal principal) {
        String email = principal.getName();
        return userRepository.findByEmail(email).getUserid();
    }
    public User callUser(Principal principal) {
        return userRepository.findByEmail(principal.getName());
    }

    public User changeUserDetails(User users) {
        User oldUser = userRepository.findByUserid(users.getUserid());
        oldUser.setUserid(users.getUserid());
        oldUser.setName(users.getName());
        oldUser.setEmail(users.getEmail());
        oldUser.setPassword(users.getPassword());
        oldUser.setName(users.getName());
        oldUser.setUsername(users.getUsername());
        oldUser.setPhonenumber(users.getPhonenumber());
        userRepository.saveAndFlush(oldUser);
        return oldUser;
    }
}
