package com.ecom.shopping.service;

import com.ecom.shopping.modal.Products;
import com.ecom.shopping.repository.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class ProductsService {
    @Autowired
    ProductsRepository productsRepository;
    public List<Products> show()
    {
        return productsRepository.findAll();
    }
    public Products showbyid(int id)
    {
        return productsRepository.findById(id);
    }
    public List<Products> showbycategory(String category)
    {
        return productsRepository.findByCategory(category);
    }
    public Products put(Products products)
    {
        return productsRepository.save(products);
    }

    public String delete(int id) {
        productsRepository.deleteById(id);
        return "deleted";
    }
    public String deleteall() {
        productsRepository.deleteAll();
        return "deleted";
    }
    public List<Products> getProductByCategoryAndPrice(String category,double min,double max)
    {
        return productsRepository.findByCategoryAndPriceBetween(category,min,max);
    }
    public List<Products> getProductByPrice(double min,double max)
    {
        return productsRepository.findByPriceBetween(min,max);
    }
    public Products changeProductDescriptions(Products products) {
        Products oldProduct = productsRepository.findById(products.getId());
        oldProduct.setId(products.getId());
        oldProduct.setName(products.getName());
        //oldProduct.setImage(products.getImage());
        oldProduct.setPrice(products.getPrice());
        oldProduct.setCategory(products.getCategory());
        oldProduct.setDescription(products.getDescription());
        productsRepository.saveAndFlush(oldProduct);
        return oldProduct;
    }

    public Set<Products> getSearchedData(String searchedItem) {
        List<Products> productsList = productsRepository.findAll();
        Set<Products> result = new HashSet<>();

        for(int i=0; i<productsList.size(); i++) {
            if(productsList.get(i).getName().toLowerCase().contains(searchedItem.toLowerCase()) ||
                    productsList.get(i).getCategory().toLowerCase().contains(searchedItem.toLowerCase()) ||
                    productsList.get(i).getDescription().toLowerCase().contains(searchedItem.toLowerCase())) {

                result.add(productsList.get(i));
            }
        }
        //System.out.println("Search result yha tak");
        return result;
    }
    public List<Products> getProductList() { return productsRepository.findAll(); }
}
